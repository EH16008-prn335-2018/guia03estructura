#include <stdio.h>
#include <stdlib.h>

int main() {
    int a,b;
    int *pa,*pb;
    int posibilidad,suma,resta;
        printf("Ingrese el primer valor:");
    scanf("%d",&a);
    pa=&a;
    
    printf("Ingrese el segundo valor:");
    scanf("%d",&b);
    pb=&b;
    printf("\n\n");
    printf("    MENU DE LA APP\n");
    printf("1.Ingresar los valores nuevamente\n2.Calcular la suma\n3.Calcular la resta\n4.Mostrar la direccion de memoria de cada valor\n0.Salir\n");
    scanf("%d",&posibilidad);
    
    while (posibilidad!=0) {
        switch(posibilidad) {
            case 1:
                printf("Ingrese el primer valor:");
                scanf("%d",&a);
                pa=&a;

                printf("Ingrese el segundo valor:");
                scanf("%d",&b);
                pb=&b;
                printf("\n\n");
                printf("    MENU DE LA APP\n");
                printf("1.Ingresar los valores nuevamente\n2.Calcular la suma\n3.Calcular la resta\n4.Mostrar la direccion de memoria de cada valor\n0.Salir\n");
                scanf("%d",&posibilidad);
                break;
                
            case 2:
                printf("\nSUMA:");
                suma=*pa+*pb;
                printf("\nLa suma de %d+%d es igual a : %d\n\n",*pa,*pb,suma);
                printf("    MENU DE LA APP\n");
                printf("1.Ingresar los valores nuevamente\n2.Calcular la suma\n3.Calcular la resta\n4.Mostrar la direccion de memoria de cada valor\n0.Salir\n");
                scanf("%d",&posibilidad);
                break;
                
            case 3:
                printf("\nRESTA:");
                resta=*pa-*pb;
                printf("\nLa resta de %d-%d es igual a : %d\n\n",*pa,*pb,resta);
                printf("    MENU DE LA APP\n");
                printf("1.Ingresar los valores nuevamente\n2.Calcular la suma\n3.Calcular la resta\n4.Mostrar la direccion de memoria de cada valor\n0.Salir\n");
                scanf("%d",&posibilidad);    
                break;
                
            case 4:
                printf("\nDIRECCIONES DE MEMORIA:");
                printf("\n%d Direccion: %p",*pa,pa);
                printf("\n%d Direccion: %p\n\n",*pb,pb);
                printf("    MENU DE LA APP\n");
                printf("1.Ingresar los valores nuevamente\n2.Calcular la suma\n3.Calcular la resta\n4.Mostrar la direccion de memoria de cada valor\n0.Salir\n");
                scanf("%d",&posibilidad);
                break;
                
            case 0:
                printf("\nSalio del programa");
                break;
                
            default:
                printf("\nIngrese nuevamente\n\n");
                printf("    MENU DE LA APP\n");
                printf("1.Ingresar los valores nuevamente\n2.Calcular la suma\n3.Calcular la resta\n4.Mostrar la direccion de memoria de cada valor\n0.Salir\n");
                scanf("%d",&posibilidad);
                break;
            }
        

    }

    return 0;
}
